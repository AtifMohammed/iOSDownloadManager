//
//  MHDownloadManager.swift
//  MHDownloadManager
//
//  Created by atif on 03/04/18.
//  Copyright © 2018 Zemoso. All rights reserved.
//

import UIKit

private protocol MHDownloadProtocol {
    func startDownload() -> String?
    func pauseDownload() -> Bool
    func resumeDownload() -> Bool
    func cancelDownload() -> Bool
}

class MHDownloadDelegate: MHDownloadProtocol {
    
    private let callbackDelegate: DownloadCallbackDelegate
    
    init(downloadCallback delegate : DownloadCallbackDelegate){
        self.callbackDelegate = delegate
    }
    
    //MARK: Download Delegate
    
    func startDownload() -> String? {
        return ""
    }
    
    func pauseDownload() -> Bool {
        return true
    }
    
    func resumeDownload() -> Bool {
        return true
    }
    
    func cancelDownload() -> Bool {
        return true
    }
    
    //MARK: Private Methods
    
    
}
