//
//  ViewController.swift
//  MHDownloadManager
//
//  Created by atif on 03/04/18.
//  Copyright © 2018 Zemoso. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DownloadCallbackDelegate{
    
    
    //MARK: View Item Variables
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var downloadProgressBar: UIProgressView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var cancelDownloadButton: UIButton!
    
    //MARK: Private Variables
    private var mDownloadState: DownloadState = .Default
    private var mDownloadManager: MHDownloadDelegate? = nil
    private var mDownloadFile: String? = nil
    
    //MARK: View Controllers

    override func viewDidLoad() {
        super.viewDidLoad()
        mDownloadManager = MHDownloadDelegate(downloadCallback: self)
        downloadButton.addTarget(self, action: #selector(self.startDownload(sender:)), for: .touchUpInside)
        cancelDownloadButton.addTarget(self, action: #selector(self.cancelDownload(sender:)), for: .touchUpInside)
    }

    //MARK: Private Methods
    
    @objc private func startDownload(sender: UIButton) {
        if(mDownloadState == .Default || mDownloadState == .Failed){
            mDownloadFile = mDownloadManager?.startDownload()
            if (mDownloadFile != nil){
                mDownloadState = .InProgress
                switchButtonState(sender: sender)
            }
        }
    }
    
    @objc private func pauseDownload(sender: UIButton) {
        if(mDownloadState == .InProgress && mDownloadManager!.pauseDownload()){
            mDownloadState = .Paused
            switchButtonState(sender: sender)
        }
    }
    
    @objc private func resumeDownload(sender: UIButton) {
        if(mDownloadState == .Paused && mDownloadManager!.resumeDownload()){
            mDownloadState = .InProgress
            switchButtonState(sender: sender)
        }
    }
    
    @objc private func cancelDownload(sender: UIButton) {
        if(mDownloadState == .InProgress && mDownloadManager!.cancelDownload()){
            mDownloadState = .Failed
            switchButtonState(sender: sender)
        }
    }
    
    private func switchButtonState(sender: UIButton){
        if(sender.isEqual(downloadButton)){
            switch mDownloadState{
            case .InProgress :
                downloadButton.setTitle("PAUSE", for: .normal)
                downloadButton.addTarget(self, action: #selector(self.pauseDownload(sender:)), for: .touchUpInside)
                break
            case .Paused :
                downloadButton.setTitle("RESUME", for: .normal)
                downloadButton.addTarget(self, action: #selector(self.resumeDownload(sender:)), for: .touchUpInside)
                break
            case .Failed :
                downloadButton.setTitle("RETRY", for: .normal)
                downloadButton.addTarget(self, action: #selector(self.startDownload(sender:)), for: .touchUpInside)
                break
            default :
                downloadButton.setTitle("DOWNLOAD", for: .normal)
                downloadButton.addTarget(self, action: #selector(self.startDownload(sender:)), for: .touchUpInside)
                break
            }
        }else if(sender.isEqual(cancelDownloadButton)){
            downloadButton.setTitle("RETRY", for: .normal)
            downloadButton.addTarget(self, action: #selector(self.startDownload(sender:)), for: .touchUpInside)
        }
    }
    
    //MARK: Enums
    
    private enum DownloadState {
        case Default
        case InProgress
        case Paused
        case Failed
    }

}

